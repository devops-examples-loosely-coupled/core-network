variable "transit" {
  type = map(object({
    cloud          = string,
    name           = string,
    transit_cidr   = string,
    region         = string,
    asn            = number,
    ha_gw          = optional(bool),
    segmentation   = optional(bool),
    firenet        = optional(bool),
    egress_enabled = optional(bool),
    az_support     = optional(bool),
  }))

  default = {
    region1 = {
      cloud        = "aws",
      name         = "lc-transit1-aws"
      transit_cidr = "10.1.0.0/23",
      region       = "eu-central-1",
      asn          = 65201,
      #firenet      = true
    },
    region2 = {
      cloud        = "azure",
      name         = "lc-transit1-azure"
      transit_cidr = "10.1.2.0/23",
      region       = "West Europe",
      asn          = 65202,
      #firenet      = true
    },
    region3 = {
      cloud        = "aws",
      name         = "lc-transit2-aws"
      transit_cidr = "10.1.4.0/23",
      region       = "us-east-1",
      asn          = 65203,
      #firenet      = true
    },    
    region4 = {
      cloud        = "aws",
      name         = "lc-transit3-aws"
      transit_cidr = "10.1.6.0/23",
      region       = "us-east-2",
      asn          = 65204,
      #firenet      = true
    },        
  }
}

locals {
  transit = defaults(var.transit, {
    segmentation   = true
    firenet        = false
    egress_enabled = true
    az_support     = true
    ha_gw          = false
  })

  firewall_image = {
    aws   = "",
    azure = "Check Point CloudGuard IaaS Single Gateway R80.40 - Pay As You Go (NGTP)"
  }

  account = {
    aws   = "AWS",
    azure = "Azure"
  }
}
